from setuptools import setup

setup(
    name='Hackerboards-Utils',
    version='0.1',
    packages=['hackerboards_utils'],
    install_requires=[
        'pyyaml>=6',
        'tabulate',
        'humanize'
    ],
    entry_points={
        'console_scripts': [
            'hackerboards-bulk=hackerboards_utils.bulk:main'
        ]
    }
)
